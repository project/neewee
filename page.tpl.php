<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    
    <!-- BLUEPRINT CSS STYLES -->
    	<link rel="stylesheet" href="<?php print base_path() . path_to_theme() ?>/blueprint/screen.css" type="text/css" media="screen, projection" />
	<link rel="stylesheet" href="<?php print base_path() . path_to_theme() ?>/blueprint/print.css" type="text/css" media="print" /> 
			<!--[if IE]>
			  <link rel="stylesheet" href="http://www.island94.org/wp-content/themes/neewee/blueprint/ie.css" type="text/css" media="screen, projection" />
			<![endif]-->	
		<!-- END BLUEPRINT CSS STYLES -->

    <?php print $styles ?>
    
    <?php if ($logo) : ?><!-- necessary for header logo -->
    <style type="text/css"> 
    	#header {
				background:transparent url("<?php print check_url($logo) ?>") no-repeat 4px 0px;
			}
    </style>
    <?php endif; ?>
    
    <?php print $scripts ?>

  </head>
  <body>
		<div class="container">
			<div id="navigation"> <!-- main navigation links -->
				<?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
		
				<!-- <div id="subscribe">
							<a href="http://www.island94.org/feed/">Subscribe to RSS</a>
				</div> -->
			</div>

			<div id="header">
				<h1><a href="<?php print check_url($front_page) ?>"><?php print check_plain($site_name) ?></a></h1>
				<h2><?php print check_plain($site_slogan) ?></h2>
				<div id="search-box"><?php print $search_box ?></div>
			</div>

			<hr/>
			
			<div id="content" class="span-13 append-1"> <!-- MAIN CONTENT -->
		
        <?php print $breadcrumb; ?>
        <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
        <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
        
        <?php if (!$node): ?>
        	<?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
        <?php endif; ?>
        
        <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
        <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
        <?php if ($show_messages && $messages): print $messages; endif; ?>
        <?php print $help; ?>
        <div class="clear-block">
          <?php print $content ?>
        </div>
		
			</div>

			<div id="sidebar" class="span-10 last">
		
				<div class="span-10" id="tabs">
					<?php print $sidebar_wide ?>
				</div>
				
				<div class="column span-5 append-1">
					<?php print $sidebar_left ?>
				</div>
				
				<div class="column span-4 last">
					<?php print $sidebar_right ?>
				</div>

			</div>

			<hr/>

			<div id="footer">
				<?php print $footer_message . $footer ?>
			</div>
		</div>
	<?php print $closure ?>
	</body>
</html>
