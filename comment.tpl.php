<?php
?>
<div class="comment <?php print $status ?> <?php print ($comment->new) ? ' comment-new' : ''; print ($comment->status == COMMENT_NOT_PUBLISHED) ? ' comment-unpublished' : ''; print ' '. $zebra; ?>">

  <?php if ($submitted): ?>
    <span class="comment-metadata"><?php print $submitted; ?></span>
  <?php endif; ?>

		<div class="comment-box">
			<div class="comment-content-top"></div>
			<div class="comment-content">
				<?php print $content ?>
			</div>
		</div>
		
</div> <!-- close comment -->