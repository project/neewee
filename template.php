<?php

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' › ', $breadcrumb) .'</div>';
  }
}

/**
 * Allow themable wrapping of all comments.
 */
function phptemplate_comment_wrapper($content, $node) {
  if (!$content || $node->type == 'forum') {
    return '<div id="comments">'. $content .'</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}



/** 
 * Creates text for link to a node's comment
 */
function newee_comment_count($node = NULL) {
	if ($node->comment) {
	  if (user_access('access comments')) {
	    $all = comment_num_all($node->nid);
	
	    if ($all) {
	      return l(format_plural($all, '1 comment', '@count comments'), "node/$node->nid", array('title'=>'comments', 'fragment' => 'comments'));
				
			}
			else {
				return l('No comments', "node/$node->nid", array('title'=>'comments', 'fragment' => 'comments'));
			}
		}
	}
}


/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  return menu_primary_local_tasks();
}


/**
 * Retheme Node $submitted
 */
function phptemplate_node_submitted($node) {
  return t('!datetime by !username',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created),
    ));
}

/**
 * Retheme Comment $submitted
 */
function phptemplate_comment_submitted($comment) {
  return t('!username on @datetime',
    array(
      '!username' => theme('username', $comment),
      '@datetime' => format_date($comment->timestamp)
    ));
}
