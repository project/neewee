<?php
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php print $picture ?>

  <?php if ($submitted): ?>
  	<div class="node-metadata">
  		<?php if ($comment_link = newee_comment_count($node)): ?><span class="comments"><?php print $comment_link ?></span><?php endif;?>
  		<?php print $submitted; ?>
  	</div>
  <?php endif; ?>

  <h3 class="node-title" ><a href="<?php print $node_url ?>" title="<?php print $title ?>" rel="bookmark"><?php print $title ?></a></h3>

	<?php if ($taxonomy): ?><div class="node-metadata"><span class="terms">Categories: <?php print $terms ?></span></div><?php endif;?>

  <div class="content clear-block">
    <?php print $content ?>
  </div>

  <div class="clear-block">
		<!-- HIDE THE LINKS
	    <?php if ($links): ?>
	      <div class="links"><?php print $links; ?></div>
	    <?php endif; ?>
	   -->
  </div>

</div>
